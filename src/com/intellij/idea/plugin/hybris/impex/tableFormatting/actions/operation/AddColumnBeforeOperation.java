package com.intellij.idea.plugin.hybris.impex.tableFormatting.actions.operation;

import com.intellij.idea.plugin.hybris.impex.tableFormatting.ImpexTableEditor;

/**
 * @author Aleksandr Nosov <nosovae.dev@gmail.com>
 */
public class AddColumnBeforeOperation extends AbstractOperation {


    public AddColumnBeforeOperation(final ImpexTableEditor editor) {
        super(editor);
    }

    @Override
    protected void perform() {
    }

}

package com.intellij.idea.plugin.hybris.impex.tableFormatting.actions.operation;

import com.intellij.idea.plugin.hybris.impex.tableFormatting.ImpexTableEditor;


/**
 * @author Aleksandr Nosov <nosovae.dev@gmail.com>
 */
public class FormatAllTablesOperation extends AbstractOperation {

    public FormatAllTablesOperation(final ImpexTableEditor editor) {
        super(editor);
    }

    @Override
    protected void perform() {
    }

}

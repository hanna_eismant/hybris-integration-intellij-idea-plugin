/*
 * This file is part of "hybris integration" plugin for Intellij IDEA.
 * Copyright (C) 2014-2016 Alexander Bartash <AlexanderBartash@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.intellij.idea.plugin.hybris.project.components;

import com.intellij.idea.plugin.hybris.ant.HybrisAntBuildListener;
import com.intellij.idea.plugin.hybris.common.services.CommonIdeaService;
import com.intellij.idea.plugin.hybris.common.services.VersionSpecificService;
import com.intellij.idea.plugin.hybris.common.utils.HybrisI18NBundleUtils;
import com.intellij.idea.plugin.hybris.common.utils.HybrisIcons;
import com.intellij.idea.plugin.hybris.statistics.StatsCollector;
import com.intellij.notification.Notification;
import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManagerAdapter;
import com.intellij.openapi.project.ProjectManagerListener;

import static com.intellij.idea.plugin.hybris.project.utils.PluginCommon.ANT_SUPPORT_PLUGIN_ID;
import static com.intellij.idea.plugin.hybris.project.utils.PluginCommon.isPluginActive;

/**
 * Created by Martin Zdarsky-Jones on 29/09/2016.
 */
public class HybrisProjectManagerListener extends ProjectManagerAdapter implements ProjectManagerListener {

    @Override
    public void projectOpened(final Project project) {
        super.projectOpened(project);
        if (isOldHybrisProject(project)) {
            showNotification(project);
        }
        registerAntListener(project);
    }

    private void registerAntListener(final Project project) {
        final CommonIdeaService commonIdeaService = ServiceManager.getService(CommonIdeaService.class);
        if (commonIdeaService.isHybrisProject(project)) {
            if (isPluginActive(ANT_SUPPORT_PLUGIN_ID)) {
                HybrisAntBuildListener.registerAntListener();
            }
        }
    }

    private boolean isOldHybrisProject(final Project project) {
        final CommonIdeaService commonIdeaService = ServiceManager.getService(CommonIdeaService.class);
        final StatsCollector statsCollector = ServiceManager.getService(StatsCollector.class);
        if (commonIdeaService.isHybrisProject(project)) {
            statsCollector.collectStat(StatsCollector.ACTIONS.OPEN_PROJECT);
            return commonIdeaService.isOutDatedHybrisProject(project);
        } else {
            final boolean potential = commonIdeaService.isPotentiallyHybrisProject(project);
            if (potential) {
                statsCollector.collectStat(StatsCollector.ACTIONS.OPEN_POTENTIAL_PROJECT);
            }
            return potential;
        }
    }

    private void showNotification(final Project project) {
        final CommonIdeaService commonIdeaService = ServiceManager.getService(CommonIdeaService.class);
        final VersionSpecificService versionSpecificService = commonIdeaService.getVersionSpecificService();

        final Notification notification = versionSpecificService.createNotification(
            "[y] project",
            HybrisI18NBundleUtils.message("hybris.project.open.outdated.title"),
            HybrisI18NBundleUtils.message("hybris.project.open.outdated.text"),
            NotificationType.INFORMATION,
            HybrisIcons.HYBRIS_ICON
        );
        notification.setImportant(true);
        Notifications.Bus.notify(notification, project);
    }
}

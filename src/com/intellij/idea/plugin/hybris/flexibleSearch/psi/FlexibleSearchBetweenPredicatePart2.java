// This is a generated file. Not intended for manual editing.
package com.intellij.idea.plugin.hybris.flexibleSearch.psi;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface FlexibleSearchBetweenPredicatePart2 extends PsiElement {

  @NotNull
  List<FlexibleSearchRowValuePredicand> getRowValuePredicandList();

}
